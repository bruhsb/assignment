
# Random Passwd used to create the app user on CloudSQL
resource "random_password" "csql_user_passwd" {
  length  = 12
  special = false
}

# Store Passwd on Secret Manager
resource "google_secret_manager_secret" "csql_user_passwd" {
  provider   = google-beta
  depends_on = [google_project_service.apis]
  secret_id  = "csql_user_passwd"
  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret_version" "csql_user_passwd_data" {
  provider    = google-beta
  secret      = google_secret_manager_secret.csql_user_passwd.name
  secret_data = random_password.csql_user_passwd.result
}

# Creating Access for computer enginers
resource "google_secret_manager_secret_iam_member" "compute" {
  provider   = google-beta
  secret_id  = google_secret_manager_secret.csql_user_passwd.id
  role       = "roles/secretmanager.secretAccessor"
  member     = "serviceAccount:${data.google_project.project.number}-compute@developer.gserviceaccount.com"
  depends_on = [google_secret_manager_secret.csql_user_passwd]
}


## Module to create a service account used by Google Cloud SQL Proxy
# Docs: https://github.com/terraform-google-modules/terraform-google-service-accounts

module "csql-sa" {
  source        = "terraform-google-modules/service-accounts/google"
  version       = "~> 3.0"
  project_id    = var.gcp_project_id
  prefix        = var.gcp_project_name
  names         = ["cloud-sql-proxy"]
  generate_keys = true
  project_roles = [
    "${var.gcp_project_id}=>roles/cloudsql.client",
  ]
}
