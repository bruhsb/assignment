# Chart Helm for install Google Cloud SQL Proxy
# Docs: https://github.com/rimusz/charts/tree/master/stable/gcloud-sqlproxy

## Google Cloud SQL Proxy image
## ref: https://cloud.google.com/sql/docs/mysql/sql-proxy
## ref: https://cloud.google.com/sql/docs/postgres/sql-proxy

locals {
  envs_csqlproxy = {
    instanceShortName = "${var.gcp_project_name}-${random_id.csql_name.hex}"
    PROJECT           = var.gcp_project_id
    REGION            = var.region
  }
}

data "template_file" "csqlproxy_envs" {
  template = file("${path.module}/helmfiles/csqlproxy.tpl.yaml")
  vars     = local.envs_csqlproxy
}

resource "helm_release" "cloudsqlproxy" {
  name          = "csql-pg"
  namespace     = kubernetes_namespace.cloudsqlproxy.metadata.0.name
  chart         = "gcloud-sqlproxy"
  repository    = "https://charts.rimusz.net"
  version       = "0.22.1"
  reuse_values  = false
  recreate_pods = false
  force_update  = false
  values        = [data.template_file.csqlproxy_envs.rendered]


  set {
    name  = "fullnameOverride"
    value = "csql-proxy"
  }

  set {
    name  = "serviceAccountKey"
    value = base64encode(module.csql-sa.key)
  }

  depends_on = [module.csql-pg]

}

resource "kubernetes_namespace" "cloudsqlproxy" {
  metadata {
    annotations = {}
    labels = {
      role = "services"
      tool = "cloudsqlproxy"
    }
    name = "cloudsqlproxy"
  }
}
