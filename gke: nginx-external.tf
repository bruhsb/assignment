# Chart Helm for install Nginx Ingress Controller
# https://github.com/helm/charts/tree/master/stable/nginx-ingress

resource "helm_release" "external_lb" {
  name          = "ext-lb"
  namespace     = kubernetes_namespace.ingress.metadata.0.name
  chart         = "ingress-nginx"
  repository    = "https://kubernetes.github.io/ingress-nginx"
  version       = "4.0.6"
  reuse_values  = false
  recreate_pods = false
  force_update  = false
  # values        = [data.template_file.nginx_envs.rendered]

  set {
    name  = "controller.ingressClass"
    value = "external"
  }

  set {
    name  = "controller.service.annotations.cloud\\.google\\.com/load-balancer-type"
    value = "external"
  }

  set {
    name  = "controller.service.loadBalancerIP"
    value = google_compute_address.external_ingress.address
  }
}

# data "template_file" "nginx_envs" {
#   template = "${file("${path.module}/helmfiles/nginx.tpl.yaml")}"
#   vars     = var.envs_nginx
# }

resource "kubernetes_namespace" "ingress" {
  metadata {
    annotations = {}
    labels = {
      role = "services"
      tool = "ingress"
    }
    name = "ingress"
  }
}

# Create random external static IP
resource "google_compute_address" "external_ingress" {
  project      = var.gcp_project_id
  name         = "external-ingress"
  description  = "External ingress endpoint"
  address_type = "EXTERNAL"
}