# ##########################
# ### GENERAL  VARIABLES ###
# ##########################


################################
#### Project & General Configs

variable "gcp_project_id" {
  type        = string
  description = "GCP project id that will be used"
}

variable "gcp_project_name" {
  type        = string
  description = "GCP project name that will be used"
  default     = "assignment"
}

variable "region" {
  type        = string
  description = "GCP region, e.g. southamerica-east1"
  default     = "europe-west4"
}

variable "zone" {
  type        = string
  description = "GCP zone, e.g. us-east1-b (which must be in gcp_region)"
  default     = "europe-west4-c"
}

variable "zones" {
  type        = list(string)
  description = "GCP zone, e.g. us-east1-b, us-east1-c (which must be in gcp_region)"
  default = [
    "europe-west4-b",
    "europe-west4-c",
    "europe-west4-a",
  ]
}

variable "gcp_credentials_json" {
  type        = string
  description = "File for access to GCP Project"
  default     = "./key.json"
}

## -> GCP APIs to enable services.
### Ref. https://github.com/terraform-google-modules/terraform-google-sql-db#enable-apis

locals {
  apis_gcp = toset([
    "compute.googleapis.com",
    "container.googleapis.com",
    "servicenetworking.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "vpcaccess.googleapis.com",
    "iap.googleapis.com",
    "servicemanagement.googleapis.com",
    "storage-api.googleapis.com",
    "sqladmin.googleapis.com"

  ])
}


variable "domain" {
  type        = string
  description = "A domain to set in Load Balancer HTTP"
  default     = "app.com"
}

#######################################
#### GKE Cluster Kubernetes Configs

variable "gke_cluster_name" {
  type        = string
  description = "The name of the cluster"
}

variable "gke_release_channel" {
  type        = string
  description = "(Beta) The release channel of this cluster. Accepted values are UNSPECIFIED, RAPID, REGULAR and STABLE. Defaults to UNSPECIFIED."
}

variable "gke_version" {
  type        = string
  description = "Version of Kubernetes / GKE to be used"
}

variable "regional_cluster" {
  type        = bool
  default     = false
  description = "Whether is a regional cluster (zonal cluster if set false. WARNING: changing this after cluster creation is destructive!)"
}

variable "master_global_access" {
  type        = bool
  description = "(Beta) Whether the cluster master is accessible globally (from any region) or only within the same region as the private endpoint."
  default     = false
}

variable "master_authorized_networks" {
  type        = list(any)
  default     = []
  description = "List of master authorized networks. If none are provided, disallow external access (except the cluster node IPs, which GKE automatically whitelists)."
}

variable "node_pools_gke_oauth_scopes" {
  type = map(any)
  default = {
    all = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}

variable "node_pools_gke_metadata" {
  type = map(any)
  default = {
    all = {}
  }
}

variable "node_pools_gke_labels" {
  type = map(any)
  default = {
    all = {}
  }
}

variable "node_pools_gke_tags" {
  type = map(any)
  default = {
    all = [
      "kubernetes",
    ]
  }
}

variable "node_pools_gke" {
  type = list(any)
}

########################################################
###
### General Services Inside GKE
###
########################################################

######### 
#### MONITORING SERVICES
#########

## -> Prometheus Operator #############

variable "envs_prometheus" {
  type = map(any)
  default = {
    grafana_url = ""
  }
}

variable "enabled_prometheus" {
  type    = bool
  default = true
}

#######################################
#### Cloud SQL - PostgreSQL

// Principal configurations

variable "csql_instance_tier" {
  type        = string
  default     = "db-f1-micro"
  description = "The tier for the master instance. db-custom-{NUMBER_OF_CPUS}-{MEMORY_IN_MIB} Ex: 2 CPU, 6,5GB (=6,5*1024=6656MiB) ram -> db-custom-2-6656"
}

# References: https://cloud.google.com/sql/docs/mysql/admin-api/rest/v1beta4/SqlDatabaseVersion
variable "csql_postgres_version" {
  type        = string
  description = "The database version to use"
  default     = "POSTGRES_13"
}

variable "csql_ha_type" {
  type        = string
  default     = "REGIONAL"
  description = "The availability type for the master instance.This is only used to set up high availability for the PostgreSQL instance. Can be either ZONAL or REGIONAL."
}

variable "csql_deletion_protection" {
  type        = bool
  default     = false
  description = "Instances Protected against deletions."
}

variable "csql_maintenance_window_day" {
  type        = number
  default     = 1
  description = "The day of week (1-7) for the master instance maintenance."
}

variable "csql_maintenance_window_hour" {
  type        = number
  default     = 23
  description = "The hour of day (0-23) maintenance window for the master instance maintenance."
}

variable "csql_maintenance_window_update_track" {
  type        = string
  default     = "stable"
  description = "The update track of maintenance window for the master instance maintenance.Can be either canary or stable."
}

variable "csql_principal_authorized_networks" {
  type        = list(any)
  description = "The ip range to allow connecting from/to Cloud SQL"
  default = [
    {
      name  = "master-cidr"
      value = "192.10.10.10/32"
    },
  ]
}

variable "csql_public_ip" {
  type        = bool
  description = "Enable the public IPv4 address assigned for the instances"
  default     = false
}

variable "csql_require_ssl" {
  type        = bool
  description = "Enable SSL"
  default     = true
}

variable "csql_user_labels" {
  type        = map(any)
  description = "The key/value labels for the instances."
  default = {
    role = "principal"
    app  = "pact-broker"
  }
}

# -> Database backups

variable "csql_backup_config" {
  description = "The backup_configuration settings subblock for the database setings"
  default = {
    binary_log_enabled             = false
    enabled                        = true
    location                       = "us"
    point_in_time_recovery_enabled = true
    start_time                     = "00:30"
    transaction_log_retention_days = null
    retained_backups               = 365
    retention_unit                 = "COUNT"
  }
}

# -> Database Configs

variable "csql_db_name" {
  type        = string
  description = "The name of the default database to create"
  default     = "app"
}

# -> Users Database Configs

variable "csql_user_name" {
  type        = string
  description = "The name of the default user"
  default     = "app"
}


# -> locals values
locals {
  gke_endpoint           = module.gke.endpoint
  gke_ca_certificate     = module.gke.ca_certificate
  gke_master_location    = module.gke.location
  sc_fast_persistent     = var.regional_cluster ? kubernetes_storage_class.fast-persistent-replicated.id : kubernetes_storage_class.fast-persistent.id
  sc_standard_persistent = var.regional_cluster ? kubernetes_storage_class.standard-persistent-replicated.id : kubernetes_storage_class.standard-persistent.id
  cslq_authorized_networks = [
    {                     # ip_cidr_range of GKE: PODs
      name  = "ALL"       #data.google_compute_subnetwork.subnet_local.secondary_ip_range[0].range_name
      value = "0.0.0.0/0" #data.google_compute_subnetwork.subnet_local.secondary_ip_range[0].ip_cidr_range
    },
  ]
}