
# Module to create a Local VPC
## Docs: https://github.com/terraform-google-modules/terraform-google-network
module "network" {
  source     = "terraform-google-modules/network/google"
  version    = "~> 3.4"
  depends_on = [google_project_service.apis]

  project_id   = var.gcp_project_id
  network_name = var.gcp_project_name
  routing_mode = "GLOBAL"

  subnets = [
    {
      subnet_name   = "subnet-01"
      subnet_ip     = "10.1.0.0/20"
      subnet_region = var.region
    },
  ]
  secondary_ranges = {
    subnet-01 = [
      {
        range_name    = "pods-subnet"
        ip_cidr_range = "192.167.0.0/20"
      },
      {
        range_name    = "services-subnet"
        ip_cidr_range = "192.168.0.0/20"
      },
    ]
  }
}

# Private IP block for private services connection
resource "google_compute_global_address" "private_ip_block" {
  depends_on    = [google_project_service.apis]
  name          = "private-ip-block"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  ip_version    = "IPV4"
  prefix_length = 20
  network       = module.network.network_self_link
}

# Private services connection for Private CloudSQL Instances
## Docs:  https://cloud.google.com/sql/docs/postgres/configure-private-services-access
resource "google_service_networking_connection" "pvt_vpc_connect" {
  network                 = module.network.network_self_link
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_block.name]
}


