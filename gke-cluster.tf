# module for GKE - Google Kubernetes Enginer
## Docs: https://github.com/terraform-google-modules/terraform-google-kubernetes-engine/tree/master/modules/beta-private-cluster
## Docs: GKE inside SharedVPC - https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-shared-vpc

module "gke" {
  source     = "terraform-google-modules/kubernetes-engine/google//modules/beta-private-cluster"
  version    = "~> 17.1"
  depends_on = [module.network, google_project_service.apis]

  name                              = var.gke_cluster_name
  project_id                        = var.gcp_project_id
  kubernetes_version                = var.gke_version
  regional                          = var.regional_cluster
  release_channel                   = var.gke_release_channel
  region                            = var.region
  zones                             = var.zones
  network                           = var.gcp_project_name
  subnetwork                        = "subnet-01"
  ip_range_pods                     = "pods-subnet"
  ip_range_services                 = "services-subnet"
  add_cluster_firewall_rules        = true
  http_load_balancing               = true
  horizontal_pod_autoscaling        = true
  network_policy                    = true
  remove_default_node_pool          = true
  grant_registry_access             = true
  disable_legacy_metadata_endpoints = true
  enable_private_endpoint           = false
  enable_private_nodes              = true
  enable_shielded_nodes             = true
  dns_cache                         = false # BETA
  service_account                   = "create"
  maintenance_start_time            = "02:00"
  master_global_access_enabled      = var.master_global_access
  master_authorized_networks        = var.master_authorized_networks
  node_pools_oauth_scopes           = var.node_pools_gke_oauth_scopes
  node_pools_metadata               = var.node_pools_gke_metadata
  node_pools_labels                 = var.node_pools_gke_labels
  node_pools_tags                   = var.node_pools_gke_tags
  node_pools                        = var.node_pools_gke
}
