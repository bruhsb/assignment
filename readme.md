## Project Terraform Introduction
In this folder are the terraform codes responsible for setting up an environment in Google Cloud.

---
## Cloud Requirements

The items below are required prior to executing these codes.
- Service Account with premises to manage the following resources in Cloud:
  - Google Compute Engine
  - Compute Network
  - Cloud SQL
  - Google Kubernetes Enginer

- A project is needed, with an active billing account.
  - Fill a value for `gcp_project_id` with the Project ID

---

## Architecture Overview

This is the prospect of an environment

![Architecture Overview](architecture.png)

---

## Terraform Requirements

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | ~> 3.90 |
| <a name="requirement_google-beta"></a> [google-beta](#requirement\_google-beta) | ~> 3.90 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | ~> 2.2 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.6 |
| <a name="requirement_random"></a> [random](#requirement\_random) | ~> 3.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | ~> 3.90 |
| <a name="provider_google-beta"></a> [google-beta](#provider\_google-beta) | ~> 3.90 |
| <a name="provider_helm"></a> [helm](#provider\_helm) | ~> 2.2 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | ~> 2.6 |
| <a name="provider_random"></a> [random](#provider\_random) | ~> 3.1 |
| <a name="provider_template"></a> [template](#provider\_template) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_csql-pg"></a> [csql-pg](#module\_csql-pg) | GoogleCloudPlatform/sql-db/google//modules/postgresql | 7.0.0 |
| <a name="module_csql-sa"></a> [csql-sa](#module\_csql-sa) | terraform-google-modules/service-accounts/google | ~> 3.0 |
| <a name="module_gke"></a> [gke](#module\_gke) | terraform-google-modules/kubernetes-engine/google//modules/beta-private-cluster | ~> 17.1 |
| <a name="module_network"></a> [network](#module\_network) | terraform-google-modules/network/google | ~> 3.4 |

## Resources

| Name | Type |
|------|------|
| [google-beta_google_secret_manager_secret.csql_user_passwd](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/resources/google_secret_manager_secret) | resource |
| [google-beta_google_secret_manager_secret_iam_member.compute](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/resources/google_secret_manager_secret_iam_member) | resource |
| [google-beta_google_secret_manager_secret_version.csql_user_passwd_data](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/resources/google_secret_manager_secret_version) | resource |
| [google_compute_address.external_ingress](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_global_address.private_ip_block](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_global_address) | resource |
| [google_project_service.apis](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_service) | resource |
| [google_service_networking_connection.pvt_vpc_connect](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_networking_connection) | resource |
| [helm_release.cloudsqlproxy](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.external_lb](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [kubernetes_namespace.cloudsqlproxy](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [kubernetes_namespace.ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [kubernetes_storage_class.fast](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/storage_class) | resource |
| [kubernetes_storage_class.fast-persistent](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/storage_class) | resource |
| [kubernetes_storage_class.fast-persistent-replicated](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/storage_class) | resource |
| [kubernetes_storage_class.standard-persistent](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/storage_class) | resource |
| [kubernetes_storage_class.standard-persistent-replicated](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/storage_class) | resource |
| [random_id.csql_name](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |
| [random_password.csql_user_passwd](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [google-beta_google_project.project](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/data-sources/google_project) | data source |
| [google_client_config.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/client_config) | data source |
| [template_file.csqlproxy_envs](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_csql_backup_config"></a> [csql\_backup\_config](#input\_csql\_backup\_config) | The backup\_configuration settings subblock for the database setings | `map` | <pre>{<br>  "binary_log_enabled": false,<br>  "enabled": true,<br>  "location": "us",<br>  "point_in_time_recovery_enabled": true,<br>  "retained_backups": 365,<br>  "retention_unit": "COUNT",<br>  "start_time": "00:30",<br>  "transaction_log_retention_days": null<br>}</pre> | no |
| <a name="input_csql_db_name"></a> [csql\_db\_name](#input\_csql\_db\_name) | The name of the default database to create | `string` | `"app"` | no |
| <a name="input_csql_deletion_protection"></a> [csql\_deletion\_protection](#input\_csql\_deletion\_protection) | Instances Protected against deletions. | `bool` | `false` | no |
| <a name="input_csql_ha_type"></a> [csql\_ha\_type](#input\_csql\_ha\_type) | The availability type for the master instance.This is only used to set up high availability for the PostgreSQL instance. Can be either ZONAL or REGIONAL. | `string` | `"REGIONAL"` | no |
| <a name="input_csql_instance_tier"></a> [csql\_instance\_tier](#input\_csql\_instance\_tier) | The tier for the master instance. db-custom-{NUMBER\_OF\_CPUS}-{MEMORY\_IN\_MIB} Ex: 2 CPU, 6,5GB (=6,5*1024=6656MiB) ram -> db-custom-2-6656 | `string` | `"db-f1-micro"` | no |
| <a name="input_csql_maintenance_window_day"></a> [csql\_maintenance\_window\_day](#input\_csql\_maintenance\_window\_day) | The day of week (1-7) for the master instance maintenance. | `number` | `1` | no |
| <a name="input_csql_maintenance_window_hour"></a> [csql\_maintenance\_window\_hour](#input\_csql\_maintenance\_window\_hour) | The hour of day (0-23) maintenance window for the master instance maintenance. | `number` | `23` | no |
| <a name="input_csql_maintenance_window_update_track"></a> [csql\_maintenance\_window\_update\_track](#input\_csql\_maintenance\_window\_update\_track) | The update track of maintenance window for the master instance maintenance.Can be either canary or stable. | `string` | `"stable"` | no |
| <a name="input_csql_postgres_version"></a> [csql\_postgres\_version](#input\_csql\_postgres\_version) | The database version to use | `string` | `"POSTGRES_13"` | no |
| <a name="input_csql_principal_authorized_networks"></a> [csql\_principal\_authorized\_networks](#input\_csql\_principal\_authorized\_networks) | The ip range to allow connecting from/to Cloud SQL | `list(any)` | <pre>[<br>  {<br>    "name": "master-cidr",<br>    "value": "192.10.10.10/32"<br>  }<br>]</pre> | no |
| <a name="input_csql_public_ip"></a> [csql\_public\_ip](#input\_csql\_public\_ip) | Enable the public IPv4 address assigned for the instances | `bool` | `false` | no |
| <a name="input_csql_require_ssl"></a> [csql\_require\_ssl](#input\_csql\_require\_ssl) | Enable SSL | `bool` | `true` | no |
| <a name="input_csql_user_labels"></a> [csql\_user\_labels](#input\_csql\_user\_labels) | The key/value labels for the instances. | `map(any)` | <pre>{<br>  "app": "pact-broker",<br>  "role": "principal"<br>}</pre> | no |
| <a name="input_csql_user_name"></a> [csql\_user\_name](#input\_csql\_user\_name) | The name of the default user | `string` | `"app"` | no |
| <a name="input_domain"></a> [domain](#input\_domain) | A domain to set in Load Balancer HTTP | `string` | `"app.com"` | no |
| <a name="input_enabled_prometheus"></a> [enabled\_prometheus](#input\_enabled\_prometheus) | n/a | `bool` | `true` | no |
| <a name="input_envs_prometheus"></a> [envs\_prometheus](#input\_envs\_prometheus) | n/a | `map(any)` | <pre>{<br>  "grafana_url": ""<br>}</pre> | no |
| <a name="input_gcp_credentials_json"></a> [gcp\_credentials\_json](#input\_gcp\_credentials\_json) | File for access to GCP Project | `string` | `"./key.json"` | no |
| <a name="input_gcp_project_id"></a> [gcp\_project\_id](#input\_gcp\_project\_id) | GCP project id that will be used | `string` | n/a | yes |
| <a name="input_gcp_project_name"></a> [gcp\_project\_name](#input\_gcp\_project\_name) | GCP project name that will be used | `string` | `"assignment"` | no |
| <a name="input_gke_cluster_name"></a> [gke\_cluster\_name](#input\_gke\_cluster\_name) | The name of the cluster | `string` | n/a | yes |
| <a name="input_gke_release_channel"></a> [gke\_release\_channel](#input\_gke\_release\_channel) | (Beta) The release channel of this cluster. Accepted values are UNSPECIFIED, RAPID, REGULAR and STABLE. Defaults to UNSPECIFIED. | `string` | n/a | yes |
| <a name="input_gke_version"></a> [gke\_version](#input\_gke\_version) | Version of Kubernetes / GKE to be used | `string` | n/a | yes |
| <a name="input_master_authorized_networks"></a> [master\_authorized\_networks](#input\_master\_authorized\_networks) | List of master authorized networks. If none are provided, disallow external access (except the cluster node IPs, which GKE automatically whitelists). | `list(any)` | `[]` | no |
| <a name="input_master_global_access"></a> [master\_global\_access](#input\_master\_global\_access) | (Beta) Whether the cluster master is accessible globally (from any region) or only within the same region as the private endpoint. | `bool` | `false` | no |
| <a name="input_node_pools_gke"></a> [node\_pools\_gke](#input\_node\_pools\_gke) | n/a | `list(any)` | n/a | yes |
| <a name="input_node_pools_gke_labels"></a> [node\_pools\_gke\_labels](#input\_node\_pools\_gke\_labels) | n/a | `map(any)` | <pre>{<br>  "all": {}<br>}</pre> | no |
| <a name="input_node_pools_gke_metadata"></a> [node\_pools\_gke\_metadata](#input\_node\_pools\_gke\_metadata) | n/a | `map(any)` | <pre>{<br>  "all": {}<br>}</pre> | no |
| <a name="input_node_pools_gke_oauth_scopes"></a> [node\_pools\_gke\_oauth\_scopes](#input\_node\_pools\_gke\_oauth\_scopes) | n/a | `map(any)` | <pre>{<br>  "all": [<br>    "https://www.googleapis.com/auth/cloud-platform"<br>  ]<br>}</pre> | no |
| <a name="input_node_pools_gke_tags"></a> [node\_pools\_gke\_tags](#input\_node\_pools\_gke\_tags) | n/a | `map(any)` | <pre>{<br>  "all": [<br>    "kubernetes"<br>  ]<br>}</pre> | no |
| <a name="input_region"></a> [region](#input\_region) | GCP region, e.g. southamerica-east1 | `string` | `"europe-west4"` | no |
| <a name="input_regional_cluster"></a> [regional\_cluster](#input\_regional\_cluster) | Whether is a regional cluster (zonal cluster if set false. WARNING: changing this after cluster creation is destructive!) | `bool` | `false` | no |
| <a name="input_zone"></a> [zone](#input\_zone) | GCP zone, e.g. us-east1-b (which must be in gcp\_region) | `string` | `"europe-west4-c"` | no |
| <a name="input_zones"></a> [zones](#input\_zones) | GCP zone, e.g. us-east1-b, us-east1-c (which must be in gcp\_region) | `list(string)` | <pre>[<br>  "europe-west4-b",<br>  "europe-west4-c",<br>  "europe-west4-a"<br>]</pre> | no |

## Outputs

No outputs.