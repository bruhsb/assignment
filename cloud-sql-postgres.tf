
# Random ID used to create the CloudSQL Name
resource "random_id" "csql_name" {
  byte_length = 4
}

# Module for CloudSQL - PostgreSQL
## Docs: https://github.com/terraform-google-modules/terraform-google-sql-db/tree/master/modules/postgresql

module "csql-pg" {
  source  = "GoogleCloudPlatform/sql-db/google//modules/postgresql"
  version = "7.0.0"
  depends_on = [
    google_project_service.apis,                         # enable GCP apis
    random_password.csql_user_passwd,                    # create the user passwd
    google_service_networking_connection.pvt_vpc_connect # create the private services connection
  ]

  // >> → Principal Configurations ← <<

  name                            = "${var.gcp_project_name}-${random_id.csql_name.hex}"
  project_id                      = var.gcp_project_id
  database_version                = var.csql_postgres_version
  region                          = var.region
  tier                            = var.csql_instance_tier
  zone                            = var.zones[0]
  availability_type               = var.csql_ha_type
  maintenance_window_day          = var.csql_maintenance_window_day
  maintenance_window_hour         = var.csql_maintenance_window_hour
  maintenance_window_update_track = var.csql_maintenance_window_update_track
  user_labels                     = var.csql_user_labels
  backup_configuration            = var.csql_backup_config
  deletion_protection             = var.csql_deletion_protection

  ip_configuration = {
    ipv4_enabled        = var.csql_public_ip
    require_ssl         = var.csql_require_ssl
    private_network     = module.network.network_self_link
    authorized_networks = var.csql_principal_authorized_networks
  }

  database_flags = [
    {
      name  = "autovacuum",
      value = "on"
    },
    {
      name  = "hot_standby_feedback",
      value = "on"
    },
    { # https://cloud.google.com/sql/docs/troubleshooting#unknown-error
      name  = "checkpoint_completion_target",
      value = "1"
    },
    { # https://cloud.google.com/sql/docs/troubleshooting#unknown-error
      name  = "checkpoint_timeout",
      value = "8000"
    },
  ]

  # -> Database Configs
  db_name              = var.csql_db_name
  db_charset           = ""
  db_collation         = ""
  additional_databases = []

  # -> Users Database Configs
  user_name        = var.csql_user_name
  user_password    = random_password.csql_user_passwd.result
  additional_users = []

  // >> →  Read Replica Configurations ← <<

  read_replica_name_suffix = "-"
  read_replicas = [
    {
      name                = "0"
      zone                = "${var.region}-a"
      tier                = var.csql_instance_tier
      ip_configuration    = local.read_replica_ip_configuration
      database_flags      = [{ name = "autovacuum", value = "off" }]
      disk_autoresize     = null
      disk_size           = null
      disk_type           = "PD_HDD"
      user_labels         = var.csql_user_labels
      encryption_key_name = null
    },
  ]
}

locals {
  read_replica_ip_configuration = {
    ipv4_enabled        = var.csql_public_ip
    require_ssl         = var.csql_require_ssl
    private_network     = module.network.network_self_link
    authorized_networks = var.csql_principal_authorized_networks
  }
}
