# *Main Providers and pre-requisites

data "google_client_config" "default" {}

data "google_project" "project" {
  provider = google-beta
}

terraform {
  required_version = ">= 1.0"
  required_providers {

    google-beta = "~> 3.90" # https://github.com/terraform-providers/terraform-provider-google-beta/releases
    google      = "~> 3.90" # https://github.com/terraform-providers/terraform-provider-google/releases
    random      = "~> 3.1"  # https://github.com/hashicorp/terraform-provider-random/releases
    kubernetes  = "~> 2.6"  # https://github.com/terraform-providers/terraform-provider-kubernetes/releases
    helm        = "~> 2.2"  # https://github.com/hashicorp/terraform-provider-helm/releases

  }
}

provider "google-beta" {

  credentials = var.gcp_credentials_json
  project     = var.gcp_project_id
  region      = var.region
  zone        = var.zone
}

provider "google" {

  credentials = var.gcp_credentials_json
  project     = var.gcp_project_id
  region      = var.region
  zone        = var.zone
}

provider "kubernetes" {

  host                   = "https://${local.gke_endpoint}"
  cluster_ca_certificate = base64decode(local.gke_ca_certificate)
  token                  = data.google_client_config.default.access_token
}

provider "helm" {

  kubernetes {
    host                   = "https://${local.gke_endpoint}"
    cluster_ca_certificate = base64decode(local.gke_ca_certificate)
    token                  = data.google_client_config.default.access_token
  }
}


## -> GCP APIs to enable services.
resource "google_project_service" "apis" {

  for_each           = local.apis_gcp
  service            = each.value
  project            = var.gcp_project_id
  disable_on_destroy = false
}